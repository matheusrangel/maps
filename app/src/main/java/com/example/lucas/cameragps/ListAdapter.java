package com.example.lucas.cameragps;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Matheus Rangel on 02/07/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListHolder> {
    public TextView mTextView;
    public Iterator<Avaliacao> avaliacoes;
    ArrayList<Avaliacao> list = new ArrayList<Avaliacao>();
    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.avaliacao_item, parent, false);
        ListHolder vh = new ListHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ListHolder holder, int position) {
        Avaliacao avaliacao = list.get(position);
        holder.render(avaliacao);
    }

    @Override
    public int getItemCount() {
        Iterator<Avaliacao> avaliacoes = Avaliacao.findAll(Avaliacao.class);
        int count = (int) Avaliacao.count(Avaliacao.class,null, null);
        if( count > 0) {
            while (avaliacoes.hasNext()) {
                list.add(avaliacoes.next());
            }
        }

        return count;
    }
}