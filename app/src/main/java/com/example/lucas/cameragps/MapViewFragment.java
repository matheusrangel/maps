package com.example.lucas.cameragps;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by Matheus Rangel on 02/07/2017.
 */

public class MapViewFragment extends Fragment {
    MapView mMapView;
    private GoogleMap googleMap;
    public LocationListener listener;
    public double latitude;
    public double longitude;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflat and return the layout
        View v = inflater.inflate(R.layout.activity_main, container,
                false);
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.getMarkers();

        return v;
    }

    public void getMarkers(){
        googleMap = mMapView.getMap();

        // create marker
        Iterator<Avaliacao> avaliacoes = Avaliacao.findAll(Avaliacao.class);
        int i = 0;
        while (avaliacoes.hasNext()) {
            Avaliacao avaliacao = avaliacoes.next();
            String nome = avaliacao.descricao + " - Nota: " + avaliacao.rate;
            MarkerOptions marker = new MarkerOptions()
                    .position(new LatLng(-26.9187+(i*0.0020), -49.066))
                    .title(nome);
            marker.icon(this.getColor(avaliacao.tipo));
            googleMap.addMarker(marker);
            i++;
        }

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(-26.9187, -49.066)).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public BitmapDescriptor getColor(String tipo){
        if(Objects.equals(tipo, getResources().getString(R.string.lanche))){
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
        }else if(Objects.equals(tipo, getResources().getString(R.string.entrada))){
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
        }else if(Objects.equals(tipo, getResources().getString(R.string.pratoPrincipal))){
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
        }else if(Objects.equals(tipo, getResources().getString(R.string.sobremesa))){
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE);
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        this.getMarkers();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
