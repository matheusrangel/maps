package com.example.lucas.cameragps;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.annotation.Target;

/**
 * Created by Matheus Rangel on 02/07/2017.
 */

public class ListHolder extends RecyclerView.ViewHolder {
    public Context context;

    public TextView avaliacaoData;
    public TextView descricao;
    public TextView tipo;
    public TextView data;
    public ImageView foto;

    public ListHolder(View v) {
        super(v);
        context = itemView.getContext();
        avaliacaoData = (TextView) itemView.findViewById(R.id.tvAvaliacaoData);
        descricao = (TextView) itemView.findViewById(R.id.tvDescricao);
        tipo = (TextView) itemView.findViewById(R.id.tvTipo);
        data = (TextView) itemView.findViewById(R.id.tvData);
        foto = (ImageView) itemView.findViewById(R.id.ivFoto);
    }

    public void render(Avaliacao avaliacao){
        this.avaliacaoData.setText("Avaliação: " + avaliacao.rate);
        this.descricao.setText("Descrição: " + avaliacao.descricao);
        this.tipo.setText("Tipo: " + avaliacao.tipo);
        this.data.setText("Data: " + avaliacao.date);
        if(!avaliacao.foto.isEmpty()){
            Uri imageUri = Uri.parse(avaliacao.foto);
            try {

                InputStream inputStream = context.getContentResolver().openInputStream(imageUri);

                Bitmap image = BitmapFactory.decodeStream(inputStream);

                this.foto.setImageBitmap(image);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(context, "Unable to open image", Toast.LENGTH_LONG).show();
            }
        }
    }


}
