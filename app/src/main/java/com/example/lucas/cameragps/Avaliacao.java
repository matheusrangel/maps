package com.example.lucas.cameragps;

import com.orm.SugarRecord;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lucas on 28/06/2017.
 */

public class Avaliacao extends SugarRecord {

    private static final int DATABASE_VERSION = 4;

    String descricao;
    String tipo;
    String rate;
    String foto;
    double latitude;
    double longitude;
    String date;

    public Avaliacao() {

    }

    public Avaliacao(String descricao, String tipo, String rate, String foto, double latitude, double longitude) {
        this.descricao = descricao;
        this.tipo = tipo;
        this.rate = rate;
        this.foto = foto;
        this.latitude = latitude;
        this.longitude = longitude;
        Date data = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        this.date = dateFormat.format(data);
    }
}
